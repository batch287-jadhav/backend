let trainer = {
	name: 'Lucifer',
	age: 19,
	pokemons: ['Pikachu','Squirttle','Draganite','Charizard','Bulbasore'],
	Friends : {
		hoen: ["May","Max"],
		kanto: ["Brock","Misty"]
	},

	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer['pokemons']);
console.log("Result of talk method:");
trainer.talk();

	function Pokemon(name,level){
		this.Name = name;
		this.Level = level;
		this.Health = 5*level;
		this.Attack = 2*level;

		this.faint = function(target){
			console.log(target.Name + " has fainted.");
		};

		this.tackle = function(target){

			console.log(this.Name + " tackled " + target.Name + "!");
			target.Health = target.Health - this.Attack;
			console.log(target.Name + "'s health is now reduced to " + target.Health + ".");
			if(target.Health <= 0){
				this.faint(target);
			}
		}
	};

	let pikachu = new Pokemon("Pikachu", 12);
	let geodude = new Pokemon("Geodude", 8);
	let mewto = new Pokemon("Mewto", 100);

	console.log(pikachu);
	console.log(geodude);
	console.log(mewto);

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewto.tackle(geodude);
	console.log(geodude);




































