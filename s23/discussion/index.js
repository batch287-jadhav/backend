console.log("Hello World!");

// [SECTION] Objects
	// An object is a datatype that is used to represent real world objects
	// Information stored in objects are represented in a "key:value" pair

	// Creating object using object initializers/literal notation

	// Syntax:
	/*
	let objectName = {
		keyA: valueA,
		keyB: valueB,

	};
	*/

	let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: 1999
	};

	console.log('Creating object using object initializers/literal notation:');
	console.log(cellphone);
	console.log(typeof cellphone);

	// Creating object using a constructor function

		// creates a reusable function to create several objects that have the same data structure

		// Syntax:
		/*
		function objectName(keyA,keyB){
			this.keyA = keyA,
			this.keyB = keyB
		}
		*/
		
		// This is an object
		// the "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor function's parameters 

	function Laptop(name, manufactureDate){
		this.name1 = name;
		this.manufactureDate1 = manufactureDate;
	};

	// The "new" operator creates an instance of an object
	// Unable to call the new operator will return undefined
	// The behaviour for this is like calling/invoking the Laptop function instead of creating a new object instance.

	let laptop = new Laptop('Lenovo', 2008);
	console.log('Result of creating object using a constructures: ');
	console.log(laptop);

	let myLaptop = new Laptop('Mac', 2020);
	console.log('Result of creating object using a constructures: ');
	console.log(myLaptop);

	let oldLaptop = new Laptop('portal R2E', 1980);
	console.log('Result of creating object using a constructures: ');
	console.log(oldLaptop);

	// Create empty objects

	let computer  = {};
	let mycomputer  = new Object();

	// [SECTION] Accesing Object properties

		// Using the dot notation 
		console.log('Result from dot notation; ' + myLaptop.name1);

		// Using the square bracket notation
		console.log('Result from square bracket notation: ' + myLaptop['manufactureDate1']);

	// Accesing array Objects

		// Accesing array elements can be also done using square brackets 

	let array = [laptop, myLaptop];
	console.log(array);

	console.log(array[0]['name1']);
	// This tells us that array[0] is an object by using the dot notation
	console.log(array[0].manufactureDate1);

	// Initiaizing/Adding object properties using dot notation

	let car = {};

	car.name = 'Honda City';
	console.log('Result from adding object properties using dot notation');
	console.log(car);

	// Initiaizing/Adding object properties using bracket notation

	car['manufacture date'] = 2023;
	console.log(car);
	console.log(car.manufacturedate);
	console.log('Result from adding object properties using bracket notation');
	console.log(car);

	// Deleting object properties

	delete car['manufacture date'];
	console.log("Result from deleting properties: ");
	console.log(car);

	// Reassigning object Properties
	car.name = 'Dodge charger R/T';
	console.log('Result from reassigning properties');
	console.log(car);

// [SECTION] Object Methods
	// A method is a function which is a property of an object

	let person = {
		name: 'John',
		talk: function(){
			console.log('Hello my name is ' + this.name);
		}
	};

	console.log(person);
	console.log('Result from object method:');
	person.talk();

	// Methods are useful for creating reusable functions that perform tasks related to object

	let friend  = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: "Austin",
			country: "Texas"
		},
		email: ['joe@mail.com','joesmail@mail.xyz'],
		introduce: function(){
			console.log('Hello my name is ' + this.firstName + " " + this.lastName);
		},

		introduceAddress: function(){
			console.log('I live at ' + this.address.city + ", " + this.address.country);
		},

		introduceContactDetails: function(){
			console.log('You can connect with me on my mails using ' + this.email[0] + " or " + this.email[1]);
		}
	};

	friend.introduce();

	// Mini-Acitivity,
	// Generate a function introduceAddress, that basicaly inform joe's address
	// Generate a function introduceContactDetails, That basicaly inform Joe's contact details
	// Trigger both function

 	friend.introduceAddress();
 	friend.introduceContactDetails();

 	// [SECTION] Real world applicatin of objets

 		/*
 		Scenario:
 			1. We would like to create a game that would have several pokemon interact with each other
 			2. Every pokemon would have the same set of stats, properties and functions
 		*/

 	// Using objects literals to create multiple kinds of pokemon would be time consuming

 	let myPokemon = {
 		name: 'Pikachu',
 		level: 3,
 		health: 100,
 		attack: 50,
 		tackle: function(){
 			console.log("This Pokemon tacked targetPokemon");
 			console.log("targetPokemon's health is now reduced to _targetPokemonHealt_");
 		},
 		faint: function () {
 			console.log("Pokemon fainted!");
 		}
 	};

 	console.log(myPokemon);

 	function Pokemon(name,level){

 		// Properties
 		this.name = name;
 		this.level = level;
 		this.health = 2*level;
 		this.attack = level;

 		// Methods 
 		this.tackle = function(target){
 			console.log(this.name + " tackled " + target.name)
 			console.log("targetPokemon's health is now reduced to _targetPokemonHealt_");
 		};
 		this.faint = function(){
 			console.log(this.name + " fainted ");
 		};
 	};

 	let pikachu = new Pokemon("Pikachu", 16);
 	let rattata = new Pokemon("Rattata", 8);

 	pikachu.tackle(rattata);
 	rattata.faint();






