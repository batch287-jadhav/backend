
	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'GET',
		headers: {
			'content-Type' : 'application/json'		},
	})
	.then((response) => response.json())
	.then((json) => {
		let titles = json.map(giveTitle);

		function giveTitle(object){
			return object.title;
		}

		console.log(titles);
	});


	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'GET',
		headers: {
			'content-Type' : 'application/json'		},
	})
	.then((response) => response.json())
	.then((json) => {console.log(json);
	console.log(`The item "${json.title}" on the list has a status of ${json.status}.`)});
	


	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({
			title: "created To Do List Item",
			completed: false,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );



	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({
			dateCompleted: "pending",
			description: "To update the my to do list with a different data structure",
			status: "pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );


	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({			
			status: "complete",
			dateCompleted: "06/06/23"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE'
	})


































