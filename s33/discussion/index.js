console.log('Hello World!');

// [Section] JavaScript Synchronous vs Asynchronous
	// JavaScript is by default is a Synchronous language.

	// This can be proven when a statement has an error, JS will not proceed with the next statement

	console.log('Hellow world');
	// consol.log('hello again');
	console.log('here we are');

	for(let i=0; i<=1500; i++){
		console.log(1);
	};

	console.log(" I'm 1501");

	// Asynchronous means thata we can proceed to execute other statements, while time consuming code is running in the background

// [SECTION] Getting All post

	// The FETCH API allows us to asynchronously request for a resources (data)
	// A 'promise' is an object that represent the eventual completion (or failuare) of an asynchronous function and it's resulting value.

	// Syntax:
		// fetch("URL")
		console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// Syntax:
		// fetch("URL")
		// .then(response => {})

	// Retrives all posts following REST API (retrieve, /posts, GET)
	// By using the .then method we can now check for the status of the promise

		fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response));

		// The 'fetch' method will return a 'promise' that resolves a 'response' object
		// The 'then' method captures the 'response' object and returns another 'promise' which will eventualy be 'resolved' or 'rejected'.

		fetch('https://jsonplaceholder.typicode.com/posts')
			// use the 'json' method from the 'Response' object to convert data rerieved into JSON format to be used in our application
		.then((response) => response.json())
			// Print the converted JSON value from the "fetch" request
			// Using multiple 'then' methods creates a 'promise chain'
		.then((json) => console.log(json));

		// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
		// Used in function to indicate which portions of code should be waited for 
		// Creates an asynchronous function

		async function fetchData(){

			let result =  await fetch('https://jsonplaceholder.typicode.com/posts');

			console.log(result);

			let json = await result.json();

			console.log(json);
		};

		console.log('Running!');

// [SECTION] Getting a specific post

	// Retrieve a specific post allowing the REST API (retrieve, /posts/:id, GET)

	fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response) => response.json())
	.then((json) => console.log(json))


// [SECTION] Creating a Post

	// Syntax:
		/*
			fetch("URL", options)
			.then((response) => {})
			.then((response) => {})
		*/

	// Creates a new post following the REST API (create, /post/:id, POST)

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({
			title: "New Post",
			body: "Hellow World!",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );


// [SECTION] Updating a post using PUT Method
	// Updates a specific post following the REST API (updage, /posts/:id, PUT)

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({
			title: "Updated Post",
			body: "Hellow Again World!",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );

// [SECTION] Updating a post using PATCH method
	// Updates a specific post following REST API (updae, /posts/:id, PATCH)

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'content-Type' : 'application/json'		},
		body: JSON.stringify({			
			title: "Corrected Post"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );

	// PATCH is used to update a single/several properties
	// PUT is used to update the whole object

// [SECTION] Deleting a post
	// Deleting a specific post following the REST API (delete, /post/:id, DELETE)

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method:'DELETE'
	})

// [SECTION] Retrieving nested/related comments to posts
	// Retrieving comments for a specific post following the REST API (retrieve, /posts/:id/comments, GET)

	fetch('https://jsonplaceholder.typicode.com/posts/1/comments',{
		method: 'GET'
	})
	.then((response) => response.json())
	.then((json) => console.log(json) );


	






