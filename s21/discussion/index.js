console.log("Hellow World!");

// An array in programing is simple list of data

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

// But with arrys we can simply write our code above like this:
let studentNumbers = ['2020-1923','2020-1924','2020-1925','2020-1926','2020-1927']
 
console.log(studentNumbers);

// [SECTION] Arrays

	// Arrays are used to store multiple related values in single variable
	// we declare arrays using square brackets ([]) also known as "Array literals"

	// Common Examples of Array
	let grades = [98.5, 94.5, 78.7, 88.1];
	let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujtsu'];

	// Possible use of an Array but is not recomended
	let mixArr = [12, 'Asus', true, null, undefined, {}];

	console.log(grades);
	console.log(computerBrands);
	console.log(mixArr);

	// Alternative way to write arrays

	let myTasks = [
		'drink HTML',
		'eat javascript',
		'inhale CSS',
		'bake bootstrap'
	];

	// using a variable

	let city1 = 'Mumbai';
	let city2 = 'Delhi';
	let city3 = 'Bangalore';

	let cities = [city1, city2, city3];

		console.log(myTasks);
		console.log(cities);

// [SECTION] Length Property

	// The .length property allowa us to get and set the total number of items in array

	console.log(myTasks.length);
	console.log(cities.length);

	let blankArr = [];
	console.log(blankArr.length);

	// Length Property can also be used with string. Some array methods and properties can also be used with strings.
	let fullName = 'Jaime Noble';
	console.log(fullName.length);

	// Length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simple updating property of an array.

	myTasks.length = myTasks.length-1;
	console.log(myTasks.length);
	console.log(myTasks);

	// To delete a specific item in an array we can employ array method (which will be shown in the next session) or an algotihm (set of code to process tasks).

	// Example using decrementation
	cities.length--;
	console.log(cities);

	// we can't do the same on strings
	fullName.length = fullName.length - 1;
	console.log(fullName.length);
	fullName.length--;
	console.log(fullName);

	let theBeatles = ['john','Paul', 'Ringo', 'George'];
	console.log(theBeatles);
	theBeatles.length++;
	console.log(theBeatles);

// [SECTION] Reading from arrays

	// Accessing array elements is one of the more common tasks that we do with an array

	// Syntax:
		// arrayName[index];

	console.log(grades[3]);
	console.log(computerBrands[2]);
	console.log(grades[100]);

	let lakersLegends = [ 'Kobe', 'Shaq', 'LaBron', 'Magic', 'Kareem'];

	console.log(lakersLegends[1]); 
	console.log(lakersLegends[3]); //

	// We can also save/store array elements in another variable. 
	let currentLaker  = lakersLegends[2];
	console.log(currentLaker);

	console.log("Array before reassignment");
	console.log(lakersLegends);
	lakersLegends[2] = 'Pau Gasol';
	console.log("Array after reassignment");
	console.log(lakersLegends);

	// Accessing the last element of an array

	let bullsLegends = ['Jordan','Pippen', 'Rose', 'Kukoc'];
	console.log(bullsLegends);

	let lastElementIndex = bullsLegends.length - 1;
	console.log(bullsLegends[lastElementIndex]);
	console.log(bullsLegends[bullsLegends.length-1]);

	// Adding items into the array

	let newArr = [];
	console.log(newArr[0]); // it should be undefined

	newArr[0] = 'Cloud Strife';
	console.log(newArr);

	console.log(newArr[1]);
	newArr[1] = 'Tifa Lockhart';

	console.log(newArr);

	// MINI Activity
	// Add 'Barret Wallace' at newArr array, It should be the last index.

	newArr[newArr.length] = 'Barret Wallace';
	console.log(newArr);

// Looping over an array

	// We can use for loop to iterate over all itemes in an array.

		for(let i=0; i<newArr.length; i++){
			console.log(newArr[i]);
		};

	let numArr = [5,12,69,87,786];

	for(let i=0; i<numArr.length; i++){
		if(numArr[i]% 5 === 0){
			console.log(numArr[i] + " is divisible by 5.");
		} else{
			console.log(numArr[i] + " is not divisible by 5");
		};
	};

// [SECTION] Multidimension Arrays

	// Multidimensional array are usful for storing complex data sturcturs

	let chessBoard = [
	['a1','b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2','b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3','b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4','b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5','b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6','b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7','b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8','b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
	];

	console.log(chessBoard);

	// Accessing elements of a multidimensional arrays
	console.log(chessBoard[1][7]);

	console.log('Pawn moves to: ' + chessBoard[1][5]);


	















