console.log("Hello World!");

// Array Methods

	// JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	// Mutators Method

		// Mutator methods are functions that "mutate" or change an array after thay are created

	let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];

	// push()
		// Adds an element in the end of an array and returns the array's length
		// Syntax:
			// arrayName.push();

		console.log('current array:');
		console.log(fruits);

		// here .push() don't add any element and gives current length of array
		let fruitsLength = fruits.push();
		console.log(fruitsLength);
		console.log('Mutated array from push method');
		console.log(fruits);

		// here .push('Mango') adds mango in the array and returns the modified length of the array
		fruitsLength = fruits.push('Mango');
		console.log(fruitsLength);
		console.log('Mutated array from push method');
		console.log(fruits);

		fruits.push('Avocado','Guava');
		console.log('Mutated array from push method:');
		console.log(fruits);

	// pop()
		// Removes the last element in an array and returns the removed element
		// Syntax:
			// arrayName.pop();

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated array from pop method");
	console.log(fruits);

	// Unshift()
		// Adds one or more elements at the begining of an array 
		// it returns the modified length of array
		// Syntax:
			// arrayName.unshift('elementA');
			// arrayName.unshigt('elementA','elementB');

		let y = fruits.unshift('Lime', 'Banana');
		console.log("Mutated array from unshift method");
		console.log(fruits);
		console.log(y);

	// Shift() method
		// Removes an element at the begining of an array and returns the removed element;
		// Syntax:
			// arrayName.shift();

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log('Mutated array from shift method;');
	console.log(fruits);

	// splice()
		// simultaneously remove elements from a specified index number and adds elements
		// Returns the removed elements 
		// Syntax:
			// arrayName.splice(startingIndex, delteCount, elementsToBeAdded);

	let splice = fruits.splice(1,2,'lime','Cherry');
	console.log(splice);
	console.log('Mutated array from splice 	method');
	console.log(fruits);

	// sort()
		// Rearranges the array elements in alphanumeric order and returns it
		// Syntax:
			// arrayName.sort();

	let neworderd = fruits.sort();
	console.log(neworderd);
	console.log('Mutated array from sort method');
	console.log(fruits);

	// reverse()
		// Reverses the order of array elements 
		// Syntax:
			// arrayName.reverse();

	fruits.reverse();
	console.log('Mutated array from reverse method');
	console.log(fruits);

	// Non-Mutated Method

		// Non-Mutator methods are functions that do not modify or change an array after they are created

	let countries = ['US','PH','CAN','SG','TH','PH', 'FR','IND'];

	// indexOf()
		// Returns the index number of the first matching element found in an array
		// It is case sensitive
		// If no match found, the result will be -1.
		// Syntax: 
			// arrayName.indexOf(searchValue);

	let firstIndex = countries.indexOf('PH');
	console.log('Result of indexOf method: ' + firstIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log('Result of indexOf method: ' + invalidCountry);

	// lastIndexOf()
		// Returns the index number of the last matching found in an array
		// Syntax:
			// arrayName.lastIndexOf(searchValue);

	let lastIndex = countries.lastIndexOf('PH');
	console.log('Result of lastIndexOf method: ' + lastIndex);

	// Getting the index number starting from a specified index
	let lastIndexStart = countries.lastIndexOf('PH',4);
	console.log('Result of lastIndexOf method: ' + lastIndexStart);

	// slice()
		// Portions/slices elements from an array and returns a new array
		// Syntax:
			// arrayName.slice(startingIndex);
			// arrayName.slice(startingIndex, endingIndex);
			// does not include element with endingIndex.

	console.log(countries);
	// slicing of elements from a specified index to the last elements
	let slicedArrayA = countries.slice(2);
	console.log('Result from slice method: ');
	console.log(slicedArrayA);

	// Slicing off elements from a specified index to another index
	let slicedArrayB = countries.slice(2,4);
	console.log('Result from slice method: ');
	console.log(slicedArrayB);

	// slicing off elements starting from the last element of an array
	let slicedArrayC = countries.slice(-3);
	console.log("Result of slice method:");
	console.log(slicedArrayC);

	// toString()
		// returns an array as a string seperated by commas
		// Syntax:
			// arrayName.toString();

	let stringArray = countries.toString();
	console.log('Result of toString method: ');
	console.log(stringArray);

	// concat()
		// Combines two array and returns the combined result
		// Syntax:
			// arrayA.concat(arrayB);

	let taskArrayA = ['drink html', 'eat javascript'];
	let taskArrayB = ['inhale CSS', 'breath sass'];
	let taskArrayC = ['get git', 'be bootstrap'];

	let task = taskArrayA.concat(taskArrayB);
	console.log('Result of concat method:');
	console.log(task);

	// combining multiple arrays
	console.log('Result of concat method:');
	let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
	console.log(allTasks);

	// combine arrays with elements
	let combinedTasks = taskArrayA.concat('smell expres','throw react');
	console.log('Result of concat method: ');
	console.log(combinedTasks);

	// join()
		// Returns an array as string seperated by specified seperator string 
		// Syntax:
			// arrayName.join('seperatorString');

	let users = ['John','Jane', 'Joe', 'Robert'];

	console.log(users.join());
	console.log(users.join(""));
	console.log(users.join(" - "));

	// Iterations Methods
		// Iterations method are loops designed to perform repetitive task on arrays

	// forEach()
		// similar to a for loop tha iterates on each array element 
		// forEach() does not return anything
		// Syntax:
		/*
		arrayName.forEach(function(indivElement){
			statement
		});
		*/

	allTasks.forEach(function(task){
		console.log(task);
	});

	let filteredTasks = [];

	// Looping through all array Items
		// It's good practice to print the current element in the console when working with array iterations

	allTasks.forEach(function(task) {
		console.log(task);

		if(task.length > 10){
			console.log(task);
			filteredTasks.push(task);
		};
	});

	console.log('Result of filtered task:');
	console.log(filteredTasks);

	// map()
		// Iterates on each element and returns new array with different values depending on the result of the function's operation
		//Unlike the forEach method, the mappethod requires the use of "return " statement in order to create another array with the performed operation 
		// Syntax:
			// let/const resultArray = arrayName.map(function(indivElement));

	let numbers = [1,2,3,4,5];

	let numberMap = numbers.map(function(number){
		return number*number;
	});

	console.log("Original Array:");
	console.log(numbers);
	console.log('Result of map method:');
	console.log(numberMap);

	// map() vs forEach()

	let  numberForEach = numbers.forEach(function(number){
		return number*number; 
	});

	console.log(numberForEach); // undfined

	// every()
		// Checks if all elements in an array meet the given condition 
		// Returns a true value if all elements meet the condition and false if otherwise
		// Syntax:
			// let/const resultArray = arrayName.every(function(indivElem){
				// return expression/condition;
			// })

	let allvalid = numbers.every(function(number){
		return (number<3);
	});

	console.log('Result of every method:');
	console.log(allvalid);

	// some()
		// checks if at least one element in the array meets the condition 
		// Syntax:
		/*
		let/const resultArray = arrayName.some(function(indivElem){
			return expression/condition;
		});
		*/

		let someValid = numbers.some(function(number){
			return (number < 2);
		});

		console.log("Result of some method:");
		console.log(someValid);

		if(someValid){
			console.log('Some numbers in the array are greater than 2');
		};

	// filter()
		// Returns a new array that contains elements which meets the given condition
		// Syntax:
			/* let/const resultArray = arrayName.filter(function(indivElem){
				return expression/condition;
			});
			*/

	let filterValid = numbers.filter(function(number){
		return (number <= 2);
	});

	console.log('Result of filter method:');
	console.log(filterValid);

	// Filtering using forEach

	let filteredNumbers = [];

	numbers.forEach(function(number){
		console.log(number);

		if(number<3){
			filteredNumbers.push(number);
		};
	});

	console.log("Result of filter method:");
	console.log(filteredNumbers);

	let products = ['Mouse', 'Keyboard', 'Laptorp','Moniter'];

	// includes()
		// includes method checks if the argument passed can be found in the array
		// Syntax:
		// arraiyName.includes(<argument to find>);

	let productFound1 = products.includes('Mouse');
	console.log(productFound1);

	productFound1 = products.includes('Headsets');
	console.log(productFound1);

	// Chaining methods
		// The result of first method used on the second method untill all chained

	let filteredPoducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	});

	console.log(filteredPoducts);

	// reduce()
		// Evaluates elements from left to right and returns/reduce the array into single value
		// Syntax:
		/*
		let/count resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression/operation;
		});
		*/

		// How the 'reduce' method works
			// 1. the first/result element in the array is stored in the 'accumulator' parameter
			// 2. The second/next element in the array is stored in the 'currentValue' parameter
			// 3. An operation is performed on the two elements
			// 4. The Loop continu

	let iteration = 0;

	let reducedArray = numbers.reduce(function(x,y){

			console.warn('current iteration: ' + ++iteration);
			console.log('accumulator: ' + x);
			console.log('currentValue: ' + y);

			return x + y;
	});

	console.log('Result	of reduce method: ' + reducedArray);

	// Reducing string arrays

	let list = ['Hello','Again', 'World'];

	let reducedJoin = list.reduce(function(x,y){
		return x + ' ' + y;
	});

	console.log('Result of reduce method: ' + reducedJoin);





