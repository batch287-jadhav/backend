console.log("Kitni bar hello");

// [SECTION] syntax, statements and comments
	// JS Statements usually end with semicolon(;)
	// JS Statements in programming are instruction 
	// that we tell computer/browser to perform
	// A syntax in programming, it is the set of
	// rules that describes how statments must be constructed. 

	/*
	There are two types of comments:
		1. The single-line comment denotes by //
		2. The Multi-line comment denotes by /*
	*/

// [ SECTION ] Variables
	// It is used to contain data
	// Like a storage, container
		/* when we create variables, certain
		portions of a device's memory is given
		a 'name' that we call "variables"*/

	//[ SUB-SECTION ] Variable Declaration
	/*Tells our device/local machine tha variable name 
	  is created and is ready to store data.*/
	/*Declaring a variaable without giving it a value 
	  will automatically assign it with a value of "undefined",
	  meaning that the variable's value was not yet defined.*/

		// syntax
			// let/const variableName;
		let myVariable;

		console.log(myVariable);
		// Print myVariable in the console

		/* 
		Guides in writing variables:

			1. Use the "let" keyword followed by
			 the variable name of your chossing and use
			 the assignment operator(=) to assign the 
			 value.
			2. Variable names should start with a lowercas
			   character, or also known for camelcase.

					Ex. 
					favcolor
					collegeCourse
			3. For constant variables, use the "const" keyword
			4. Variable names should be idicative(desciptive) of
			   being stored to avoid confusion.

		*/

		// [SUB-SECTION] Initialising variables

			// syntax
				// let/const variableName = value;
			let productname = "dekstop"
			console.log(productname);

		 	let productPrice = 1899;
		 	console.log(productPrice);

		 	// usually being when a certian value is not changing.
		 	const interest = 3.53;

	 	// [SUB-SECTION] Reassigning Variable values
	 		// syntax
	 			//variable = newValue;
	 		productname = "Laptop";
	 		console.log(productname);

	 	// [var vs. let/const]
	 		/* var is also used in declaring a vaiable. But
	 		   var is an ECMAScript1(ES1) feature, in modern time, 
	 		   ES6 updates are already using let/const now.
			*/

	   		// For example 
		   		a = 5;
		   		console.log(a)
		   		var a;

	   		// let keyword
	   		   b = 5;
	   		   console.log(b);

	    //[SUB-SECTION] let/const local/global scope
	    	// Scope essentially means where these variables 
	    	// are available for use.
	    	// A block is a chunk of code bounded by {}.
	    	// A block lives in curly braces. Anything within
	    	// curly braces is a block.

	    		let outerVariable = "hello";
	    		{
	    			let innerVariable = "hello again";
	    			console.log(innerVariable);
	    		}

	    		console.log(outerVariable);

    		//[SUB-SECTION] Multiple Variable Declaraitons
    			//Multiple Variables may be declared at a time.
	    		
	    		let productCode = "DC017", productBrand = "Dell";
	    		console.log(productCode, productBrand);

	    //[SECTION] Data Types

	    	// [strings]
	    		/* Strings are a series of characters
	    		  that a word, a phrase, a sentence or
	    		  anything related to creating text.
	    		*/
	    		//we use single('') or double("") quotation.

	    		let country = "India";
	    		let province = "Maharashtra";

	    		//Concatenating Strings
	    		// Multiple strings can be combined to create a
	    		// single string using the "+" symbol

	    		let fullAddress = province + " , " + country;
	    		console.log(fullAddress);

	    		let greetings = "I live in " + country;
	    		console.log(greetings);

	    		/*Escape character (\) in string in combinations with
	    		  other characters can produce different effects*/

	    		//"\n" refers to creating a new line in between text.
	    		let mailAddress = 'Maharashtra\n\nIndia';
	    		console.log(mailAddress);

	    		let message = "John's employees went home early"
	    		console.log(message);
	    		message = 'john\'s  employees went home early';
	    		console.log(message);

    		//[numbers]
    			// for Integers/whole numbers
    			let headcount = 25;
    			console.log(headcount)

    			// for decimal numbers/fractions
    			let grade = 90.5
    			console.log(grade);

    			// Exponential Notation
    			let planetDistance = 2e10;
    			console.log(planetDistance);

    			// Combining text and strings
    			console.log("John's grade last quarter is " + grade);

			//[BOOLEAN]
				//This is the true or false values

				let isMarried = false;
				let inGoodConduct = true;

				console.log("isMarried: " + isMarried);
				console.log("inGoodConduct:" + inGoodConduct);

			//[Arrays]
				// Arrays are a special kind of data types that's used to store multiple values

				// Syntax
				// let/const arrayName = [element1, element2, ...]

				let grades = [98.7, 92.1, 90.6];
				console.log(grades);

				//valid for different data types
				let details = [ "John", 22, 's', true];
				console.log(details);

			// [Object]
				/* Objects are another special kind of data 
				  type that's used to mimic real world objects/items */

				// Syntax
				// let/const objectName = {
				//		propertyA: value,
				//		propertyB: value
				// }

				let person = {
					fullName: "Shubham Jadhav",
					age: 19,
					isMarried: true,
					contact: ["12345","67890"],
					address: {
						housenumber: "345",
						city: "Mumbai"
					}
				}
				console.log(person);

				/* typeof operator is used to determine the 
				 type of dat/value of a variable. It outputs string
				console.log(typeof grades);

				// Constant Objects and Arrays

				/*
				The keyword const is little misleading.

				It does not define a constant value. it 
				defines a constant referance to a value.

				Because of this you can not:
					Reassign a constant value
					Reassign a constant array
					Reassign a constant object

					but you cna 

					change the elements of a constant array
					change the properties of constant object
				*/

				// const anime =['one piece', 'one punch man', 'attack on titan']
				// anime = ['kimetsu ne yaiba']

				// console.log(anime);

				//but if we can try to do this,

				const anime =['one piece', 'one punch man', 'attack on titan']
				anime[1] = 'kimetsu ne yaiba';

				console.log(anime);

			// [SECTION] NULL
				// It is used to intentionaly express the 
				// absnece of a value in variable declaration/initialization.
				// If we use the null value, simply means that a data
				// type was assigned to a variable but it does not hold
				// any value/amount is nullified


				let spouse = null;


				