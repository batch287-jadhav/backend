console.log("Hello World");

// [SECTION] While loop

	// A while loop takes in an expression/condition
	// The codeblock will check the condition in each iteration and run until the condition becomes false.

	/*
	Syntax:
		while(expression/condition){
			statement;
		};
	*/

	function whileloop(){
		let count = 5;

		while(count !== 0){
		// The current value of count is printed out.
		console.log('while: ' + count);
		count--;
		// Makes sure that expression/conditions in loops have their corresponding increment/decrement oprators to stop the loop.
		};
	};
	
	// Another Example

	function gradeLevel(){
		let grade = 1;

		while(grade <= 5){
			console.log("I'm a grade " + grade + " Student!");

			grade++;
		};	
	}
	
// [SECTION] Do While Loop

	// A do-while loop works alot like a while loop, but unlike while loops, do-while loops checks condition after codeblock is run in each iteration.

	/*
	Syntax:
		do{
			statement;
		} while(expression/condition)
	*/

	function doWhile(){
		let count = 0;

		do{
			console.log('what ever happens, I will be here!');
			count--;
		} while(count>0);
	
	}
	
	function secondDoWhile(){
		let number = Number(prompt('Give me a number'));

		do{
			console.log('Do while: ' + number);
			number += 1;
		}while(number <10);	
	}
	
// [SECTION] For Loop

	// A for loop is more flexible than while and do-while loop.
	// For Loop consit of three parts:
		// 1. The "initialization" value that will track the progression of the loop
		// 2.The "expression/condition" that will be evaluated which will determine whether the loop will run one more time or not.
		// 3. The "finalExpression" indicates how to advances the loop/how the variable will behave. 

	/*
	Syntax:
		for (initialization ; expression/conditions; finalExpression){
			statement
		}
	*/

	function forLoop(){
		for (let count = 0; count<= 20; count++){
			console.log('You are currently: ' + count);
		};
	}

	// [SUB-SECTION] Loops for letters
	
	let myString = "alex";

	console.log(myString);
	console.log(myString.length);

		console.log(myString[0]);

		let x = 0;

		// we create a loop that will print out the individual letters of the muString vaiable
		for(x; x<myString.length; x++){

			// The current variable of myString is printed out using it's index value.
			console.log(myString[x]);
		};

	// Mini-Activity 
		// Create variable that contains your name
		// For every vowel in your name it should be replace with 0

		// strech Goal: Use prompt on collecting your name.

		// let name = prompt("write your name:");

		// let name = "shubham";

		// for(i=0; i<name.length; i++){
		// 	if((name[i] == 'a')||(name[i] == 'e')||(name[i] == 'i')||(name[i] == 'o')||(name[i] == 'u')){
		// 		console.log(0);
		// 		name[i] = 0;
		// 		console.log(name[i]);
		// 	} else{
		// 		console.log(name[i]);
		// 	};
		// };
		// console.log(name);

// [SECTION] Continue and Break Statements

	//  The "continue " statement allows the code to go to next iteration of the loop without finishing the execution of all statements of the code block
	// The "break" statement is used to terminate the current loop once a match has been found

		for(let count= 0; count<=20; count++){

			if(count % 2 === 0){
				continue;
			};

			console.log('continue and Break: ' + count);

			if(count > 10){
				break;
			};
		};

	// Anothe example:

		let name = "alexandro";

		for(let i = 0; i<name.length; i++){
			console.log(name[i]);

			if(name[i].toLowerCase() === 'a'){
				console.log('continue to the next iteration');
				continue;
			};

			if(name[i] == 'd'){
				break;
			};

		};



