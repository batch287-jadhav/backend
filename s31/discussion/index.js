// Use the "require" directive to lode Node.js modules
// A module is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the Hyper Text Trasfer Protocal
// The "http module" is a set of individual files that contain code to create a "component" the helps establish data transfer in applications
// http is analogas to a envolope of letter containing details of sender and reciever
// HTTP is a protocol that allows the fetching of resourcess such as HTML documents
// Clients (browser) and servers (Node.js/Express.js applications) communicate by exchanging individual messages.
	// The messages sent by the 'client', usually a web browser are called "requests"
	// The messages sent by the 'server', as an answer are called "requests"
const http = require('http');


// Using this module's createServer() method, we can create an HTTP server tha listens to requests on a specified port and gives a response back to the clients
// The HTTP module has createServer() method that accepts a function with an argument and allows for a cration of server.
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive request from the client and send responses back to it.

http.createServer(function (request, response){

	// Use the writeHead() method to:
	// Set a status code for the response - a 200 means OK, a 404 maeans not found
	// Sets the content-type of the response as a plain text meassage
	response.writeHead(200, {'content-Type': 'text/plain'});

	// Sends the response with the text content "Hellow World"
	response.end('hellow World!');


// A port is a vitual point where a network connections start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen(4000)"" method where the server will listen to any request that are sent to it eventually communicating with our server
}).listen(4000)

// When server is runnig, console will print the message:
console.log("Server is running at localhost:4000");




































