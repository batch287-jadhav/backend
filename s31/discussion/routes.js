const http = require('http');

const port = 4004;

// Creates a variable server that stores the output of 'createServer' method.
const server = http.createServer((request,response) => {

	if (request.url == '/greetings'){
		response.writeHead(200, {'content-Type': 'text/plain'})
		response.end('Welcome to the server! This is currently running at localhost: 4004.')

	} else if (request.url == '/homepage'){
		response.writeHead(200, {'content-Type': 'text/plain'})
		response.end('Welcome to homepage! This is currently running at localhost: 4004.')
	}
	// Mini-activity, create a route that will return a message of "Page not available"

	else {
		response.writeHead(404, {'content-Type': 'text/plain'})
		response.end('Page not found!')
	}
})

// Uses the "server" and "port" variables created above.
server.listen(port);

// When the "server" is running, console will print the message:
console.log(`server is now accessible at localhost:${port}.`);









































