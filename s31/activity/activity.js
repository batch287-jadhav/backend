// 1. What directive is used by Node.js in loading the modules it needs?
/*
=> The require directive is used for loding the modules
*/


// 2. What Node.js module contains a method for server creation?
/*
=> The HTTP module is contains method for server creation
*/


// 3. What is the method of the http object responsible for creating a server using Node.js?
/*
=> In http we use createServer() to create a server using Node.js
*/


// 4. What method of the response object allows us to set status codes and content types?
/*
=> .createHead() method allows us to set status codes and content types
*/	


// 5. Where will console.log() output its contents when run in Node.js?
/*
=> It's shown in the bash giving signle that our server is running
*/


// 6. What property of the request object contains the address's endpoint?
/*
=> .end() holds the addess's endpoint
*/

