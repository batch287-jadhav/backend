const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 2000;


mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.wj3hhzy.mongodb.net/s35-activity", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;


db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => {
	console.log("We're connected to the cloud database!");
});


const userSchema = new mongoose.Schema({

	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true}));


app.post("/signup", (req,res) =>{

	User.findOne({username: req.body.username}).then((result,err) => {

		if(result != null && result.username == req.body.username){

			return res.send("User already exists!");
		} else{

			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save().then((savedUser, savedErr) => {

				if(savedErr){
					return console.log(savedErr);
				} else{
					return res.status(201).send("New user registered successfully!");
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at post ${port}.`));













