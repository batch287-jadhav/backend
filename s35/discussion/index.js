const express = require("express");

// Mongoose is a package that allows creation of Schema to model our data structure
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// [SECTION] MongoDB Connection
// Syntax:
	// mongoose.connect("<MongoDB connecting string>", {useNewUrlParser: true});

	// The connection string can be get from databas Deployment in our Mongodb atlas account and on connecting will give the string

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.wj3hhzy.mongodb.net/s35-discussion",
	{ 
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

	// The {neeUrlParses : true } allows us to avoid any current and futures errors while connecting to MongoDB
	// The { useUnifiedTopology : truev} allows us to enable the new MongoDB connection with the configuration option while establishing MongoDB server and Mongoose.

// Connection to the Database

	// Allows us to handle errors when the initial connection is established

let db = mongoose.connection

// If a connection error occured, output in console.
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is succesful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."))

// [Section] Mongoose Schemas
	
	// Schemas determine the structure of the documents to be written in the database
	// Schemas acts as blueprint to our data
	// Use the schmea() constructor of the mongoose module to create a new schma object
	// The "new" keyword creates a new Schema

const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There should a field called "name" and its datatyp is "string"
	name: String,
	status: {
		type: String,
		// Default values are the pre-determined values for a field if we don't put any value
		default: "pending"
	}
});

// [SECTION] Models

	// It uses schemas and are used to create/instantiate objects that correspond to the schema
	// It acts like as a middleman from the server (JS code) to our database
	// Server > Schema (Blueprint) > Database > Collection

const Task = mongoose.model("Task", taskSchema);

// [SECTION] Creation of todo list routes

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

	// Creating a new Task

	// Bussiness Logic
	/*
		1. Add a functionality to check if there are duplicate task 
			- If the task already exists in the databas, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body 
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema by default is for it to be "pending"
	*/

app.post("/tasks", (req,res) => {

	// Checks if there are duplicate tasks
	// "findOne" is a mongoose method that acts similar to "find" of MongoDB
	// "findOne()" return the first document that matches the search criteria as single object
	Task.findOne({name : req.body.name}).then((result,err) => {

		// If a document was found and the documents's name matches the information sent via the client/postman
		if(result != null && result.name == req.body.name){

			return res.send("Duplicate task found");
		} else {

			// Create a new task and save it to database
			let newTask = new Task({
				name: req.body.name
			});

			// "save()" method will store the information to the database
			newTask.save().then((savedTask, saveErr) => {

				// If there are errors in saving
				if(saveErr){
					// Will print any errors found in console
					return console.error(saveErr);
				} else{
					return res.status(201).send("New task created!");
				}
			})
		}
	})
})


// Getting all the task

	// Bussiness Logic

	/*
		1. Retrieve all the documents
		2. If an error is encountered, print error
		3. If no error are found, send a success status back to the client/postman and return an array of documents 
	*/

app.get("/tasks",(req,res) =>{

	// "find" is a mongoose method that is similar to MongoDB 'find', and empty "{}" means it returns all documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) =>{

		// If any error occurred
		if(err){
			return console.error(err);
		} else {

			// Return the result
			// The "json" method allows to send a JSON format for the response
			// The returned response is puposefully returned as an object with the "data" property to mirror the real world complex data structures
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at post ${port}.`));























