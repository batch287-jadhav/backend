// # Aggregration In MongoDB

	// Aggregration is clustering/consolidating of data
	// Can be used to calculate different stats based on given database

	// Aggregation Pipelines: 
		// It is acts like data frame or a specific flow of operations that processes, transforms, and returns the results.
		// The pipeline consist of multiple stages
		// Each stage performs some operation and transforms the data as it passes through it 

	// Syntax:
	/*
		db.collection.aggregate([
			{stageA},
			{stageB},
			{stageC};
		]);

	*/

	// Create documents to use for the discussion


	// Here a new collection named fruits is formed
	db.fruits.insertMany([
	  {
	    name : "Apple",
	    color : "Red",
	    stock : 20,
	    price: 40,
	    supplier_id : 1,
	    onSale : true,
	    origin: [ "Philippines", "US" ]
	  },

	  {
	    name : "Banana",
	    color : "Yellow",
	    stock : 15,
	    price: 20,
	    supplier_id : 2,
	    onSale : true,
	    origin: [ "Philippines", "Ecuador" ]
	  },

	  {
	    name : "Kiwi",
	    color : "Green",
	    stock : 25,
	    price: 50,
	    supplier_id : 1,
	    onSale : true,
	    origin: [ "US", "China" ]
	  },

	  {
	    name : "Mango",
	    color : "Yellow",
	    stock : 10,
	    price: 120,
	    supplier_id : 2,
	    onSale : false,
	    origin: [ "Philippines", "India" ]
	  }
	]);


	// Using the aggregate method
		
	/* 

		The "$match" is used to pass the documents that meet that specified conditions to the next pipeline stage/ aggregation process.

		Syntax:
			{ $match: { feild: value}}
		
		The "$group" is used to group the elements together and feild-value pairs using the data from the grouped elements

		Syntax: 
			{ $group: {_id: "value", feildResult: "valueResult"}}

		Using both "$match" and "$group" along with aggregation will find for producs that are on sale and will group the total amount of stocks for all suppliers found.

		Syntax:
			db.collectionName.aggregate([
				{ $match: {feildA, valueA}},
				{ $group: { _id: "$feildB", result: {operation}}}
		    ]);

	*/

		// Th "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
		// The "$sum" operator will total the values of all "stock" fields in the returne documents that are found using the "$match" criteria.

	db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id", total: { $sum: "$stock" }}}
	]);
	

	// Field Projection with aggregation

	// The "$project" can be used when aggregating data to include/exclude fields from the return results

	/*
		Syntax:
			{ $project: {field: 0/1}}
	*/

	db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id", total: { $sum: "$stock" }}},
		{ $project: { _id: 0}}
	]);


	// Sorting Aggregated results

		// The "$sort" can be used to change the order of aggregated results
		// The -1, means that the aggregated result will be in reverse order

		/*
			Syntax:
				{ $sort: { field: 1/-1} }
		*/

		db.fruits.aggregate([
			{ $match: { onSale: true}},
			{ $group: { _id: "$supplier_id", total: { $sum: "$stock" }}},
			{ $sort: { _id: 1}}
		]);

	// Aggregating Results based on Array Fields
		// The "$unwind" deconstruct an array field from a collection/feild with an array value to output a result for each element.

		/*
			Syntax:
				{ $unwind: field }
		*/

			db.fruits.aggregate([
				{ $unwind: "$origin" }
			]);
			

	// Output should be : display fruit documents by their origin and the kinds of fruits tha are supplied

		db.fruits.aggregate([
			{ $unwind: "$origin"},
			// { $group: { _id: "$origin", kinds:{ $sum: 1}}},
			{ $project: { origin: 1,
						  name: 1,
						  _id: 0}},
		]);
		













