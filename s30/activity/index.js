// Using count operator to count total number of fruits

	db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $count: "name"}
	]);

// Using the count operator to count the total number of fruits with stock more than or equal to 20.
	
	db.fruits.aggregate([
		{ $match: {stock: {$gte: 20}}},
		{ $count: "name"}
	]);
	
// Use the average operator to get the average price of fruits onSale per supplier.

	db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: {_id: "$supplier_id", avg_price: { $avg: "$price"}}}
		]);

// Use the max operator to get the highest price of a fruit per supplier.

	db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id", max_price: { $max: "$price"}}}
		]);

// Use the max operator to get the lowest price of a fruit per supplier.

	db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id", min_price: { $min: "$price"}}}
		]);





























