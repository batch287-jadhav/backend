// Crud Operations"
	// CRUD operations are the heart of any backed application.
	// Mastering the crud operations is essenetial for any developer.

// [SECTION] Inserting Documents (CREATE)

// Insert One Documents
	// Creating MongoDB Syntax in a text editor make it easy for us to modify and create our code as opposed to typing it diretly.
	// Syntax:
		// db.collectionName.insertOne({object})

db.users.insertOne({
    "firstName":"John",
    "lastName":"Smith"
});

db.users.insertOne({
	firstName : "Jane",
	lastName: "Doe",
	age:21,
	contact:{
		phone: "1234567890",
		email: "janedoe@email.com"
	},
	courses: ['CSS','Javascript','Python'],
	department: "none"
});

// Insert many documents

db.users.insertMany([
	{	
		firstName : "Stephen",
		lastName: "Hawking",
		age:78,
		contact:{
			phone: "1234567890",
			email: "everything@email.com"
		},
		courses: ['React','Python','PHP'],
		department: "none"
	},
	{
		firstName : "Neil",
		lastName: "Armstrong",
		age:84,
		contact:{
			phone: "1234567890",
			email: "space@email.com"
		},
		courses: ['React','Laravel','Sass'],
		department: "none"
	}
]);

// [SECTION] Finding Documents (READ)

	// Find
		// If multiple documents match the criteria only the FIRST document that matches the search term will be returned

	// Syntax:
	/*
		db.collectionName.find();
		db.collectionName.find({field: value});
	*/

	db.users.find();
	db.users.find({firstName: "Stephen"});

// [SECTION] Updating documents (UPDATE)
	
	// Updating a single Document

	db.users.insertOne({
		firstName: 'Test',
		lastName: 'Test',
		age: 0,
		contact: {
			Phone: "0000000000",
			email: "test@email.com"
		},
		course: [],
		department: 'none'
	});

	// To update firstName: "Test"
	// Syntax:
	/* 
		db.collectionName.updateOne({criteria},{ $set {feild: value}});

		db.collectionName.updateMany(
		{criteria},{ $set {feild: value}});

	*/

	db.users.updateOne(
		{firstName: "Test"},
		{
			$set:{
				firstName: 'Bill',
				lastName: 'Gates',
				age: 65,
				contact: {
					phone: '0987654321',
					email: 'bill@email.com'
				},
				course: ['PHP','C++','HTML'],
				department: "Operations",
				status: 'active'
			}
		});


	db.users.updateMany(
		{ department: "none"},
		{
			$set: {department: 'HR'}
		}
	);

// [SECTION] Deleting documents (DELETE)
	
	// Delete one

		// Syntax:
			db.collectionName.deleteOne({ criteria });

	db.users.insertOne({
		firstName: "Test"
	});

	db.users.deleteOne({
		firstName: "Test"
	});

	// Delete Many
		// Syntax:
			db.collectionName.deleteMany({ criteria });


















