console.log("Hello World!");

// Conditional Statement
	// Allows us to control the flow of our program. It allows us to run a statment/instruction if a condtion is met or run another seperate instruction if otherwise.

// [SECTION] If, Else If, and Else statement

	// IF statements

	let numA = 0;

		if(numA < 0){
			console.log("Hellow");
		};

		/*
		Syntax:

			if(condition){
				statement;
			};

		*/

		// let's update the variable and run if statement with the same condtion:

		numA = -1;

		if(numA > 0){
			console.log("Hellow again from numA is -1");
		};

		// This will not run because the expression now results to false:
		console.log(numA > 0);

		let city = "New York";

		if(city === "New York"){
			console.log('Welcome to New York city!');
		};

	// Else if Clause

		// Execute a statement if previous conditions are false and if the specified condition is truw

		let numH = 1;

		numA = 1;

		if(numA < 0){
			console.log("Hellow");
		} else if(numH > 0){
			console.log("World!");
		};

		// We were able to run the else if() statement statement after we evaluated that if condition was failed

		// If the if() condition was passed and run, we will no longer evaluate else if() and end the process there.

		let numB = 5;

		if(numB < 0){
			console.log("This will not run");			
		} else if(numB < 5){
			console.log("This value is less than 5.")
		} else if(numB < 10){
			console.log("The value is less than 10.");
		};

		// Let's now update the city variable and look at another example:

		city = "Tokyo";

		if(city === "New York"){
			console.log("Welcome to New York city!");
		} else if(city === "Tokyo"){
			console.log("Welcome to Tokyo, Japan!");
		};

	// Else Statement

		// Executes a statement if all existing conditions are false

		if(numA < 0){
			console.log("Hello!");
		} else if(numH === 0){
			console.log('World!');
		} else{
			console.log("Again!");
		};

		// Since both the preceeding if and else if conditions failed, the else statement was run instead.

		numB = 21;

		if(numB < 0){
			console.log("This will not run!");

		} else if(numB < 5){
			console.log("The value is less than 5.")
		} else if(numB < 10){
			console.log("The value is less than 10.")
		} else if(numB < 15){
			console.log("The value is less than 15.")
		} else if(numB < 20){
			console.log("The value is less than 20.")
		} else {
			console.log("The value is greater than 20!");
		};








		let message = "No Message!";
		console.log(message);

		function determineTyphoonIntentsity(windspeed){
			if(windspeed <30){
				return "Not any storm yet"
			} else if(windspeed < 61){
				return "Tropical Depression detected.";
			} else if(windspeed<=88){
				return 'Tropical strom detected';
			}  else if(windspeed<=117){
				return 'Severe Tropical storm detected';
			} else{
				'Typhoon Detected'
			}

		};

		message = determineTyphoonIntentsity(100);
		console.log(message);

		/* miniactivity
		windspeed <= 61 => Tropical Depression detected
		windspeed >= 62 && <= 88 => Tropical strom detected
		windspeed>= 89 && <= 117 => Severe Tropical storm detected
		widspeed > 118 => Typhoon Detected
		*/

	// Truthy and Falsy

		// In JS a truthy value is a value that is considered true when encountered in a Boolean context

		/*
		Falsy Values/Exception for truthy:
			1. false
			2. 0 
			3. -0
			4. ""
			5. null 
			6. undefined
			7. Nan
		*/

		if(true){
			console.log("Truthy!");
		};
		if(1){
			console.log("Truthy!");
		};
		if([]){
			console.log("Truthy!");
		};

		// False Example
		if(false){
			console.log("Falsy!");
		};
		if(0){
			console.log("Falsy!");
		};
		if(undefined){
			console.log("Falsy!");
		};

	// Conditional (Ternary) Operator

		// The Conditional Ternary Operator takesin three operands:
			// 1.conditoin
			// 2. Expression to execute if the condition is truthy
			// 3. Expression to execute if condition is falsy
		// Can be used as an alternative to an
		/*
		Syntax:
			(expression) ? ifTrue : ifFalse;
		*/

	// Single Ternary Statement Execution
		let ternayResult = (1<18) ? true : false;
		// let ternayResult = (1<18) ? false : true; 
		// will result to false
		console.log("Result of Ternay Operator: " + ternayResult);

	// Multiple Statement Execution

		let name;

		function isOfLegelAge(){
			name = "John";
			return "You are of the legel age limit";
		};

		function  isUnderAge() {
			name = "Jane";
			return "You are under the age limit";
		};

		// parseInt is used to change the datatype to int 
		let age = parseInt(prompt('what is your age?'));
		console.log(age);
		let legelAge = (age>18) ? isOfLegelAge() : isUnderAge();
		console.log("Result of Ternay Operator in functions: " + legelAge + ", " + name);

	// Switch Statement

		// The switch statement evaluates an expression and matches the expression's value to a case clause.
		// The ".toLowerCase()" is a function/method that will change the input recieved from prompt into all lowercased 

		/*
		Syntax:
			switch(expression){
				case value:
					statement;
					break;
				default
					statement;
					break;
			};
		*/

		let day = prompt("What day of the week is is today?").toLowerCase();
		console.log(day);

		switch(day){
			case 'monday':
			console.log('The color of the day is red!');
			break;
			case 'tuesday':
			console.log('The color of the day is blue!');
			break;
			case 'wednesday':
			console.log('The color of the day is orange!');
			break;
			case 'thursday':
			console.log('The color of the day is voilet!');
			break;
			case 'friday':
			console.log('The color of the day is yellow!');
			break;
			case 'saturday':
			console.log('The color of the day is red!');
			break;
			case 'sunday':
			console.log('The color of the day is red!');
			break;

			default:
			console.log('Please enter a valid day.');
			break;
		}


	// Try-Catch-Finally Statement

		// try-ctch statements are commonly used for error handaling

		function showIntensityAlert(windspeed){
			// Insert try-catch-finally statement
			try{
				alerat(determineTyphoonIntentsity(windspeed));
			} catch(error){
				console.log(typeof error);

				console.log(error.message);
			} finally {
				alert('Intensity updates will show new alert!');
				console.log('This is from finally!');

				// It will continue the execution of code regardless of success and failure of code execution in the "try" block to handle/resolve errors
			}
		};

		showIntensityAlert(35);
