// Use the require directive to load the express module/package
// Allows us to access to methods and functions that will allow us to easlily create a server
const express = require("express");

// Creates an application using express
// In layman's term, this app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

// Use middleware to allow express to read json
app.use(express.json());

// Use middleware to allow express to able to read more data types from a response
app.use(express.urlencoded({extended:true}));


// [SECTION] Routes
	// Express has methods corresponding to each HTTP method

// Here request and response are parameters so we can use other names for it but should be cosistent with it
app.get("/Hello", (request, response) => {
	response.send('Hellow from the /hello endpoint!');
})

app.post("/display-name", (req,res) => {
	console.log(req.body);
	res.send(`Hellow there ${req.body.firstName} ${req.body.lastName}!`);
});

// Sign-up in our Users.

let users = []; // this is our mockdatabase

app.post("/signup", (request,response) => {
	console.log(request.body);
	// Mini-Activity, If the body.password and body.username is empty don't include in our mockdata and if both places are not empty then add it our database

	if( request.body.username !== "" && request.body.password !== ""){

		// This will store the user object sent via Postman to the users array
		users.push(request.body)
		response.send(`Hello ${request.body.username}! you are successfully signed in!`);
	}
	else{response.send("Please enter valid details!");
	}
});

// This route expects to receive a PUT request at the URI "/chang-password"

app.put("/change-password", (req,res) =>{

	let message;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`

			console.log("Newly updated mock database:");
			console.log(users);
			break;
		} else {
			message = "User not found!"
		}
	}
	res.send(message);
})



app.listen(port, () => console.log(`Server is currently running at port ${port}`));

























