const Course = require("../models/Course");
const auth = require("../auth");
const User = require('../models/User');

/*
	module.exports.addCourse = (userId, reqBody) => {

		return User.findById(userId).then(result =>{

			if(result.isAdmin){
				let newCourse = new Course({

					name: reqBody.name,
					description: reqBody.description,
					price: reqBody.price
				});

				return newCourse.save().then((course, error) => {

					if(error) {

						return false;
					} else {

						return true;
					}
				})

			} else {

				return "You don't have this access"
			};

		
		});
	}
*/

module.exports.addCourse = (data) => {

		console.log(data);

		if(data.isAdmin){
			let newCourse = new Course({

				name: data.course.name,
				description: data.course.description,
				price: data.course.price
			});

			return newCourse.save().then((course, error) => {

				// Course creation failed
				if(error) {

					return false;

				// Course creation successful
				} else {

					return true;
				}
			});

		}; 

		// Since Promis.resolve() returns a resolved promise, the variable message will already be in a resolved status.
		let message = Promise.resolve("User must be an Admin to access this.");
		return message.then((value) => {
			return value;
		});
	
};

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {

		return result;
	});
};

module.exports.activeCourses = () => {

	return Course.find({ isActive: true}).then( result => {

		return result;
	});

};

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then( result => {
		return result;
	});
};

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};

/*
module.exports.archiveCourse = (reqParams,reqbody) => {

			if(reqbody.isAdmin){

				let updateActiveField = {
					isActive: reqbody.isActive
				}

				return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

					if(error){
						return false;
					} else {
						return true;
					};
				});
			};

			let message = Promise.resolve("User must be an Admin to access this.");
			return message.then((value) => {
				return value;
			});
};
*/

module.exports.archiveCourse = (reqParams) => {

				let updateActiveField = {
					isActive: false
				}

				return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

					if(error){
						return false;
					} else {
						return true;
					};
				});
			};

			
