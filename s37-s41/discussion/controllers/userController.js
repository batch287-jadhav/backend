const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
		return User.find({ email : reqBody.email }).then(result => {
			// The "find" method returns a recod if a match is found

				if(result.length > 0){

					return true;

				} else{

					return false;
				}
		});
}


module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
		// here using of 10 means hashing is done 10 times.
	})

	// the .save() callback function returns user and error
	return newUser.save().then((user, err)=> {

			if(err){
				return false
			} else {
				return true
			}
	});
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		if(result == null){

			return false;
		} else{

			// we now know that email is correct, is password correct also? 
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);

			// Correct password
			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			// Incorrect password
			} else {
				return false;
			};
		};
	});
};

module.exports.getProfile = (reqBody) => {

	console.log(reqBody); // will return the key/value pair of userId : data.Id

	return User.findById(reqBody.userId).then(result => {

		result.password = " ";
		return result;

		// return {
		// _id : result._id,
		// firstName : result.firstName,
		// lastName : result.lastName,
		// email : result.email,
		// password: "",
		// isAdmin: result.isAdmin,
		// mobileNo: result.mobileNo,
		// enrollments: result.enrollments
		// };	

	});
	
};

// async is getting used to change the default synchronous nature to asynchronous nature
module.exports.enroll = async(data) => {

	// 1st Await Update first the user models
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({ courseId: data.courseId});

		return user.save().then((user, error) => {

			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	// with the use of await we are trying to make the code for both the functions to run simultaneously 
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({ userId: data.userId});

		return course.save().then((course, error) => {

			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){

		return true;
	} else {
		return false;
	};
};




























