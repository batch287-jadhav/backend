const express = require('express');
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")


router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body).then(resultFromController => {

		res.send(resultFromController)
	});
});

router.post("/register", (req,res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/details", auth.verify, (req,res) => {
	// userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));

	// 
	const userData = auth.decode(req.headers.authorization); // will return the token.payload

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route for enroll user to a course

router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;



























