const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoute.js");
const courseRoutes = require('./routes/courseRoute.js')

// Allows us to control the app's Cross Origin Resource Sharing settings
// Allows our backend application to be available to our frontend application
const cors = require("cors");

// create an app variable that stores the result of the express function
const app = express();

// connect ot our MongDB database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.wj3hhzy.mongodb.net/s37-s41?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
		// useFindAndModify: false
	}
);

mongoose.connection.once("open", () => console.log("Now connected in the cloud database"))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Will used the defined port number for the application whenever the variable is available or will used port 4000 if none is defined
// Doing this syntax, it will allow flexibility when using the application 
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})

app.use("/users", userRoutes);

app.use("/courses", courseRoutes);




































