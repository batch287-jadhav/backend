console.log("Hello World!");

// Functions
	// Fuctions in JS are lines/blocks of codes that tell our device/application/browser to perform a certain task/statement when called/invoked.

	// Function Declaration
	// Defines a function with specified parameters

	// syntax:
	/*
		function function_name(){
			code block(-statement-)
		};
	*/

		function printName(){
			console.log("My name is Lucifer");
		};


		/*
			function keyword - used to defined a javascript function
			functionName - the name of function. Functions are named to be able to use later in the code.

			funcion({}) - the statements which comprises the body of the function. This is where the code to be executed.
		*/


// Function Invocation
	// The code block and statments inside a function is not immediately executed when the function is defined.

		printName();
		// Invoking/calling the functions that we declared.

// Function Declaration vs Function Expressions

	// Function Declaration
		// A function can be created through function declaration by using keyword and adding a function name.

		declaredFunction();
		// declared functions can be hoisted as long as the function has been defined.

		function declaredFunction(){
			console.log("Hello, from declaredFunction()");
		};

		declaredFunction();

	// Function Expressions 
		// A funcion can also be stored in a variable. his what we called funcion expressions.

		// Anonymous Function - A function without name

		// variableFunction();
		// the undeclared funcions/defined using variables are not allowed to hoisted.

		let variableFunction = function(){
			console.log("Hello again!!");
		};

		variableFunction();
		console.log(variableFunction); 
		// This one will work and will just only show the actual function inside variableFunction

		let funcExpression = function funcName(){
				console.log('Hellow from other side');
		};

		funcExpression();

		// Re-assignment of declared functions

			declaredFunction = function(){
				console.log('This is the updated declaredFunction');
			};

			declaredFunction();

			funcExpression = function(){
				console.log('updated funcExpression');
			};

			funcExpression();

		// Re-assign from keyword const

			const constantFunc = function(){
				console.log("Initialised with const!");
			};

			constantFunc();

			/*
			constantFunc = function(){
				console.log("This is new constantFunc");
			};  // result will be an error  
			*/

// Function Scoping 

	// Scope is the accessibility (visibility/jurisdiction/availability) of variables within our program

		/*
			JS Variables has 3 types of scopes:
				1. local/block scope
				2. global scope
				3. function scope
		*/

	{
		let localVar = "Armando";
		// local variables can't be used outside this codeblock
	}

	let globalVar = "Worldwide";

	console.log(globalVar);
	// console.log(localVar);

	// Function Scope

		// JS has funcion: Each function creates a new scope

		function showNames(){
			var functionVar = "joe";
			let funcitonLet = "jane";
			const functionConst = "john";

			console.log(functionVar);
			console.log(funcitonLet);
			console.log(functionConst);

		}

		showNames()

		var functionVar = "mike";
		let funcitonLet = "scott";
		const functionConst = "ben";

		console.log(functionVar);
		console.log(funcitonLet);
		console.log(functionConst);

	// Nested Functions

		// You can create another function inside a function.

		function myNewFunction(){
			let name = "jane";

			function nestedFunction(){
				let nestedName = "john";
				console.log(name);
				console.log(nestedName);
			};

			nestedFunction();
		};

		myNewFunction();

	// Function and Global Scoped Variables

		// Global Scoped Variable
		let golobalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = 'Renz';

			console.log(golobalName);
		};

		myNewFunction2();

// Using Alert()
	// alert() allows us to show a small window at the top of our browser page to show informatioin to our users.

		// alert() Syntax:
		// alert("<Message in String>");

		alert("Hellow!");

		function showSampleAlert(){
			alert("Have a nice day!");
		};

		showSampleAlert();

		console.log("I will only be log in the console when the alert is dismissed!.");

		// Notes on the use of alert();
			// Show only an alert() for short dialogs/messages to the user.
			// Do not overuse alert() because the program/JS has to wait for it to be dismissed before continuing.

// Using prompt()

	 // prompt() allows us to show a small window at the top of the browser to gather user input.

	 let samplePrompt = prompt("Enter your name.");

	 console.log("Hello, " + samplePrompt);

	 function printWelcomeMessage(){
	 	let firstName = prompt("Enter your first Name");
	 	let lastName = prompt("Enter your Last Name");
	 	console.log("Hello, " + firstName + " " + lastName + "!");
	 	console.log("Welcome to my page!");

	 	// for MiniActivity
	 	alert("Hello, " + firstName + " " + lastName + "!");
	 	alert("Welcome to my page")
	 };



	 printWelcomeMessage(); 

	 // Mini-Activity, after collecting the user's first name and last name, Create an alert that welcomes the user's into our page. Ofcourse, utilizing prompt accordingly.

// Function Naming Conventions
	// Function names should be definative of the task it will perform. It is usually contains a verb.

	function getcourse(){
		let courses = ['Science 101', 'Math 101', 'English 101'];
		console.log(courses);

	};

	getcourse();

	// Avoid generic names to avoid confusions within our code.

	function get(){
		let name = "Jaime";
		console.log(name);
	};

	get();

	// Avoid pointless and inappropriate function names.

	









