/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userInfo(){
		let fullname = prompt("Please enter your full name");
		let age = prompt("How old are you?");
		let location = prompt("Enter your address");

		console.log("Hello, " + fullname + "!")
		console.log("We got to know that you are " + age +" years old.");
		console.log("you live at " + location);
	};

	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function favArtists(){
		alert("please enter your favorite 5 artists");
		let artist1 = prompt("1st artist");
		let artist2 = prompt("2nd artist");
		let artist3 = prompt("3rd artist");
		let artist4 = prompt("4th artist");
		let artist5 = prompt("5th artist");

		console.log("1. " + artist1);
		console.log("2. " + artist2);
		console.log("3. " + artist3);
		console.log("4. " + artist4);
		console.log("5. " + artist5);
	}

	favArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function favMoviesAndRatings(){
		alert("Please enter your favourite movies along with their rotton tommatos ratings.");
		let movie1 = prompt("1st movie");
		let rating1 = prompt("rating of movie");
		let movie2 = prompt("2nd movie");
		let rating2 = prompt("rating of movie");
		let movie3 = prompt("3rd movie");
		let rating3 = prompt("rating of movie");
		let movie4 = prompt("4th movie");
		let rating4 = prompt("rating of movie");
		let movie5 = prompt("5th movie");
		let rating5 = prompt("rating of movie");

		console.log("1. " + movie1);
		console.log("Rotton Tommatos Rating: " + rating1 + "%");
		console.log("1. " + movie2);
		console.log("Rotton Tommatos Rating: " + rating2 + "%");
		console.log("1. " + movie3);
		console.log("Rotton Tommatos Rating: " + rating3 + "%");
		console.log("1. " + movie4);
		console.log("Rotton Tommatos Rating: " + rating4 + "%");
		console.log("1. " + movie5);
		console.log("Rotton Tommatos Rating: " + rating5 + "%");
	}

	favMoviesAndRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

  
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: ");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);
