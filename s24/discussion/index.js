console.log("Hello World!");

// [SECTION] Exponant Operator
	
	// ** acts as pow operator
	const firstNum = 8**2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);
// [SECTION] Template Literals

	// Allows us to write string without using the concatination operator(+);

	let name = 'John';

	// Pre-Template Literal String
	// Uses single quotes ('')
	let message = 'Hello ' + name + '! Welcome to programming!';
	console.log(message);

	// String using Template Literal
	// Uses backticks(``)
	message = `Hello ${name}! Welcome to programming!`;
	console.log(`Message without template literals: ${message}`);

	// Multi-line using Template Literals
	const anotherMessage = `${name} attended a math competition.
	He won it by solving a problem 8**2 with the solution of ${firstNum}.`

	console.log(anotherMessage);

		// Template literals allows us to write string with embeded JS expressions

	const interestRate = 0.1;
	const principal = 1000;

	console.log(`The intereston your saving account is: ${principal * interestRate}.`);

// [SECTION] Array Destructuring
	// Allows us to unpack elements in arrays into distinct variables

	// Syntax:
		// let/const [variableName, variableName, variableName] = array;

	const fullName = ['Juan', 'Dela', 'Cruz'];

	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! it's nice to meet you!`);

	const [firstName, middleName, lastName] = fullName;

	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// [SECTION] Object Destructuring 
	// Allows us to unpack properties of objects into distinct variables 

	// Syntax:
		// let/const {propertyName, propertyName, propertyName} = object;

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	};

	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

	// Object Destructuring

	let {givenName, maidenName, familyName} = person;

	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`);

	function getFullName({givenName, maidenName, familyName}){
		console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
	};

	getFullName(person);

// [SECTION] Arrow Function
	// Compact alternative syntax to traditional functions

	const hello = () => {
		console.log('Hello World!');
	};

	hello();

	// Mini-Activity
	// convert getFullName to arrow function

	let nameGiven = ({givenName, maidenName, familyName}) => {
		console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
	};

	nameGiven(person);

	const students = ['John', 'Jane', 'Judy'];

	// Arrow functions with loops
		// Pre-Arrow function
	students.forEach(function(student){
		console.log(`${student} is present.`);
	});

	// Arrow functions
	students.forEach((student) =>{
		console.log(`${student} is absent.`);
	});

// [SECTOIN] Implict return statement
	// There are insance when you can omit the 'return' statement

	// Arrow function

	const add = (x,y) => x + y;

	let total = add(1,2);
	console.log(total);

// [SECTION] Default function argument value
	// Provides a default argument value if none is provided when the function is invoked

	const greet = (name = 'User') =>{
		return `Good Morning, ${name}!`;
	};

	console.log(greet());
	console.log(greet("Lucifer"));

// [SECTION] Class-Based project Blueprints
	// Allows creation/instantiation of object using classes as blueprints

	// Creating a Class
		// The constructer is a special method of a class for creating/initializing an object for the class.
		// Acts as a template for JS Object

		// Syntax:
			/*
			class className{
				constructor(objectPorpertyA,objectPorpertyB){
					this.objectPorperty1 = objectPorpertyA;
					this.objectPorperty2 = objectPorpertyB;
				}
			}
			*/

	class Car{
		constructor(brand, name, year){
			this.Brand = brand;
			this.Name = name;
			this.Year = year;
		}
	}

	// Instatiating a class
		// The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties

	let myCar = new Car();

	console.log(myCar);

		myCar.Brand = 'Ford';
		myCar.Name = 'Mustang';
		myCar.Year = 2023;

	console.log(myCar);

	const myNewCar = new Car('Toyota','Innova', 2020);
	console.log(myNewCar);
























