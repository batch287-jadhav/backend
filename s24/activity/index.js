let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

let address = ['258', 'Washingto Ave NW', 'California', '90011'];
let [houseNumber, city, state, pin] = address;
console.log(`I live at ${houseNumber} ${city}, ${state} ${pin}.`);

let animal = {
	name: 'Lolong',
	type: 'saltwater',
	species: 'crocodile',
	weight: 1075,
	measures: '20 ft 3 in'
};

let {name, type, species, weight, measures} = animal;
console.log(`${name} was a ${type} ${species}. He weighted at ${weight} kgs with a measurement of ${measures}.`);

let numbers = [1,2,3,4,5];

numbers.forEach((number) =>{
	console.log(number);
});

let givesum = () =>{
	let sum = 0;
	numbers.forEach((number) => {
		sum = sum + number;
	});
	return sum;
};

let reduceNumber = givesum();
console.log(reduceNumber);


class Dog{
	constructor(name,age,breed){
		this.Name = name;
		this.Age = age;
		this.Breed = breed;
	};
};

let myDog = new Dog('Frankie',5,'Miniature Dachshund');
console.log(myDog);