console.log("Hello World!");

// [SECTION] Arithmatic Operators
	
	let x = 1_3691369;
	let y = 200_01234;
	// we can add underscore in numbers to add spacings for easy readability
	console.log(x);
	console.log(y);

	let sum = x + y;
	console.log("Result of Addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product  = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x/y;
	console.log("Result of division operator: " + quotient);

	let remainder = x%y;
	console.log("Result of modulo operator: " + remainder);

// [SECION] Assignment Operators

	// Basic Assignment Operator(=)
	/* This assignment operator assigns the value of the right 
	   operand to a variable and assigns the result to the variable*/

	 let assiggnmentNumber = 8;

	 // Addition Assignment Operator(+=)
	 /* The addition assignment operator adds the value of the right 
	 	operand to a variable and assigns the result to the variable */

	 	assiggnmentNumber = assiggnmentNumber + 2;
	 	console.log("Result of addition assignment operator: " + 
	 				assiggnmentNumber);

	 	// Shorthad for assiggnmentNumber = assiggnmentNumber + 2
	 	assiggnmentNumber += 2;
	 	console.log("Result of addition assignment operator: " + 
	 				assiggnmentNumber);

	 	// Subtraction/Multiplication/Division Assignment operator(-=,*=, /=)

	 	assiggnmentNumber -= 2;
	 	console.log("Result of subtraction assignment operator: " + 
	 				assiggnmentNumber);

	 	assiggnmentNumber *= 2;
	 	console.log("Result of multiplication assignment operator: " + 
	 				assiggnmentNumber);

	 	assiggnmentNumber /= 2;
	 	console.log("Result of division assignment operator: " + 
	 				assiggnmentNumber);

// Multiple Operators and Paranthese

	/* PEMDAS Rule (Parenthisis, Exponents, Multiplication, Division, 
					Addition and Subtraction)*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas: " + mdas);

	let pedmas = 1 + (2-3) * (4/5);

		// The order of the operations
		// 1. 2 - 3 = -1  & 4/5 = 0.8
		// 2. (-1)*0.8 = -0.8
		// 3. 1 + (-0.8) = 0.2
	console.log("Result of pedmas: " + pedmas);

// [SECION] Incriment and Decrement 
	/* Operators that adds or subtracts vaulues by 1
		and rassigns the value of the variable where the
		increment/decrement was applied to */

	let z = 1;

	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	increment = z++;
	// value of "z" is at 2 before it was incremented
	console.log("Result of post-increment: " + increment);
	// value of "z" was icreased again reassigning the value to 3
	console.log("Result of post-increment: " + z);

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);

// [SECION] Type Coercion
	// Is the automatic or implicit convesion of values from one data type to other.

	let numA = '10';
	let numB = 10;

	let coercion = numB + numA;
	console.log(coercion);
	console.log(typeof coercion);

	// Black text means that the output returned is a string data type.

	let numC = 12;
	let numD = 16;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);
		// Boolean "true" = 1 & "false" = 0
 
	let numF = false + 1;
	console.log(numF);

// [SECTION] Comparision Operator

	let juan = 'juan';

	// Equality Operator (==)
	/*
		-checks whether the operands are equal/have the 
		 same content
		- Attempts to convert and compare
		- Returns a boolean value
	*/

	console.log(1 == 1);
	console.log(1 == '1');
	console.log(0 == false);
	console.log('juan' == 'juan');
	console.log('juan' == juan);

	// Inequality Operator (!=)
	/*
		- Checks whether the operands are not equal/have
		  different content.
		- Attempts to convert and compare operands of the 
		  different data types.
		- Returns boolean value
	*/

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	console.log('juan' != 'juan');
	console.log('juan' != juan);

	// Strict Equality Operator(===)
	/*
		- Checks whether the operands are equal/have 
		  the same content
		- Compares the data types of 2 values
		- strict equality operators are better to use in
		  most cases to ensure the data types provided are correct
	*/

	console.log(1 === 1);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log('juan' === juan);

	// Strict Inequality Operator(!==)
	/*
		- Checks whether the operands are not equal/have 
		  the different content
		- Compares the data types of 2 values
		- strict equality operators are better to use in
		  most cases to ensure the data types provided are correct
	*/

	console.log(1 !== 1);
	console.log(1 !== '1');
	console.log(0 !== false);
	console.log('juan' !== 'juan');
	console.log('juan' !== juan);

// [Section] Relational Operators
	/* Some comparision operators check whether one 
	value is greater than or less than the other value*/


	let a = 50;
	let b = 65;

	// GT or Greater than Operator(>)
	let isGreaterThan = a>b;
	console.log(isGreaterThan)

	// LT or Less Than Operator(<)
	let isLessThan = a<b;
	console.log(isLessThan);

	//GTE or Greater Than or Equal Operator(>=)
	let isGTorEqual = a>=b;
	console.log(isGTorEqual);

	//LTE or Less Than or Equal Operator(<=)
	let isLTorEqual = a<=b;
	console.log(isLTorEqual);

// Logical Operators

	let isLegelAge = true;
	let isRegistered = false;

	// Logical AND (&)
	let allRequirementsMet = isLegelAge & isRegistered;
	console.log("Result of Logical AND Operator: " +
				allRequirementsMet);

	// Logical OR (||)
	let someRequirementsMet = isLegelAge || isRegistered;
	console.log("Result of Logical OR Operator: " +
				someRequirementsMet);

	// Logical NOT (!)
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of NOT operator: " + 
				someRequirementsNotMet);



