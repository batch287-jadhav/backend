// Defines when particular controllers will be used
// It contains all the endpoits for our application

// We seperate the routes such that "app.js" contains only the information on the server

// we will need to use express Router() function to achieve this
const express = require('express');

// It creates a Router instance that functions as a middleware and routing
const router = express.Router();

const taskController = require("../controllers/taskController");

// [SECTION] Routes

// Routes to get all the tasks
router.get("/", (req,res) => {

	// Invoke the "getAllTasks()" function from the "contoller.js" file and return the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a new task

router.post("/", (req,res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Delete task

router.delete("/:id", (req,res) =>{

	taskController.deleteTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

// Update Task

router.put("/:id/complete",(req,res) =>{

	taskController.updateTask(req.params.id, req.body).then( resultFromController => res.send(resultFromController));
});

module.exports = router;

router.get("/:id", (req,res) =>{
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});