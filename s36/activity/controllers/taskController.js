// Contains instruction on How your API will perfome its intended tasks
// All of the operations can be done will being placed in this file

const Task = require("../models/task");

// Controller for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and export these functions
module.exports.getAllTasks = () => {

	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskROutes.js" 
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to our client/Postman
	return Task.find({}).then( result => {
		
		// The "return" statment returns the result of the MongoDB query to the result parameter defined in the "then" method.
		return result;
	});
};

// Controller creating a task
// The request body coming from the client was passed frm "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file.
module.exports.createTask = (requestBody) => {

	// Create a task object based on the Mongoose model "Task"
	let newTask = new Task ({

		// Set the "name" property with the value received from the client/Postman
		name: requestBody.name,
		status: requestBody.status
	});

	// Saves the newly created 'newTask' object in the MongoDB database
	// The "then" method wait until task is stored in the db or an error is encountered before returning a "true" or  "false" value back to client/Postman
	// The "then" method will accept and store the following 2 arguments 
		// The first parameter will store the result returned by the Mongoose "save" method.
		// The second  parameter will store the "error" object
	return newTask.save().then((task, error) => {
		// If error is encoutered returns a "false" boolean back to the Postman
		if(error){
			console.log(error);
			return false

		// If successful, returns the new task object back to the client/Postman
		} else{
			return task 
		};
	} );
};

// Controller deleting a task

module.exports.deleteTask = (taskId) =>{

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if(err){
			console.log(err);
			return false
		} else {
			return "Deleted Task."
		};
	});
};

module.exports.updateTask = (taskId, newContent) => {

	return Task.findByIdAndUpdate(taskId).then((updatedTask, err) => {

		if(err){
			console.log(err);
			return false
		} 

		updatedTask.name = newContent.name;
		updatedTask.status = newContent.status;

		return updatedTask.save().then((result, saveErr) => {

			if(saveErr){

				console.log(saveErr);
				return false;
			} else{
				return "task updated succesfully!"
			};
		});

		
	});
};

module.exports.getTask = (taskId) => {

	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskROutes.js" 
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to our client/Postman
	return Task.findById(taskId).then( result => {
		
		// The "return" statment returns the result of the MongoDB query to the result parameter defined in the "then" method.
		return result;
	});
};


