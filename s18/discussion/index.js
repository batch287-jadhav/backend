console.log("Hello World!");

// [SECTION] Functions

	// [SUB-SECTION] Parameters and Arguments

		// Functions in JS are lines/blocks of code that tell our device/application/browser to perform a certain task when called/invoked/triggered.

		function printInput(){
			let nickname = prompt("Enter your nickname:");
			console.log("Hii, " + nickname);
		};

		// printInput();

		// For other cases, functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

		// here name only acts like a variable

		function printName(name123){
			// name123 is a parameter
			console.log("Hi, " + name123);
		};

		printName("Lucifer"); //"Lucifer" is argument.

		// You can directly pass data into the function. The function can then call/use that data which is reffered as "name123" within the function.

		// when the "printname()" function is called again, it stores the value of "Lucifer" in the parameter "name123" then uses it to print a message.

		// Vaiables can also be passed as an argument.
		let sampleVariable = "Suzume";

		printName(sampleVariable);

		// Function arguments cannot be used by a function if there are no parameters within the funcion.

		printName(); // It will be like Hi!, undefined

		function checkDivisibilityBy8(num){
			let remainder = num%8;
			console.log("The remainder of " +  num + " divided by 8 is: " + remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(15);
		checkDivisibilityBy8("24");

		// You can also do the same using prompt(), however, take note tha prompt() outputs a string. Strings are not ideal for mathematical computations.

	// Functions as Arguments

		// Function parameters can also accept other function as arguments 

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.");
		};

		function invokeFunction(funct){
			funct();
		};

		invokeFunction(argumentFunction);

		// Adding and removing parathesis "()" impacts the output of JS heavily.

		console.log(argumentFunction);
		// will provide information about the function in the console using console.log();

	// Using multiple parameters

		// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeding order.

		function createFullName(firstName,middleName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
		};

		createFullName("M", "s", "Dhoni", "7");

		createFullName("M", "S", "Dhoni");

		createFullName("MSD");

		// In JS, providing more/less arguments than the expected parameters will not return an error.

		// Using variables as Arguments

		let firstName = "Dwayne";
		let middleName = "The Rock";
		let lastName = "Johnson";

		createFullName(firstName, middleName, lastName);

// [SECTION] The Return Statement

	// The "return" statment allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

		function returnFullName(firstName, middleName, lastName){
			console.log("This message is from console.log");
			return firstName + " " + middleName + " " + lastName;
			console.log("This message is from console.log");
		};

		let completename = returnFullName("Jeffery", "smith","Bezos");
		console.log(completename);

		// In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.

		// Whatever value is returned from the "returnFullName" function is stored in the "complename" varaible.

		// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignore because it ends the function execution.

		console.log(returnFullName(firstName, middleName, lastName));
		// In this example, console.log() will print the returned value of the returnFullName() function.

		// You can also create a variable inside the function to contain the result and return that variable instead.

		function returnAddress(city,country){
			let fullAddress = city + " , " + country;
			return fullAddress;
		};

		let myAddress = returnAddress("Bombay", "India");
		console.log(myAddress);

		function printPlayerInfo(username,level,job){
			console.log("username: " + username);
			console.log("Level: " + level);
			console.log("Job: " + job);
		};

		printPlayerInfo("Knight",95 ,"paladin");
		
		let user1 = printPlayerInfo("Knight",95 ,"paladin");
		console.log(user1);

		// Return undefined because printPlayerInfo return nothing. It only consol.log the details.

		// You cannot save any value from printPlayerInfo() because it does not return anything.




