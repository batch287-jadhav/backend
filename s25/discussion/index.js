console.log("Hello World!");

// [SECTION] JSON Objects
	
	// JSON stands for Javascript Object Notation
	// It is used for data storage and data transfer
	// Syntax:
		/*
		{ 
			"PropertyA": "ValueA",
			"PropertyB": "valueB",
		}
		*/

	// JSON Objects
	/*
		{
			"city": "Mumbai",
			"state": "Maharashtra",
			"country": "India"
		}
	*/

// [SECTION] JSON Arrays

	/*
		"cities": [
		{"city": "Mumbai", "state": "Maharashtra","country": "India"},
		{"city": "Bangaluru", "state": "Karnataka","country": "India"}
		]
	*/

// [SECTION] JSON Methods
	// The JSON object contains methods for parsing and converting data into stringified JSON

	// Converting Data into stringified JSON
		// Stringified JSON is a JavaScript object converted into a single string to be used in other function of a Javascript application.

	let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];

	// The "stringify" method is used to convert JavaScript objects into a string

	console.log("Result from stringifiy method:");
	console.log(batchesArr);
	console.log(JSON.stringify(batchesArr)); 

	let data = JSON.stringify({
		name: 'Lucifer',
		age: 19,
		address: {
			city: 'Mumbai',
			country: 'India'
		}
	});

	console.log(data);

// [SECTON] Using stringify method with varaiables
	// When information is stored in a variable and is not hard coded into an object that is being stringified, We can supply the value with a variable.

	// Syntax:
	/*
		JSON.stringify({
			propertyA: "variableA",
			propertyB: "variableB",
		})
	*/

	// Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user details

	// User details
	/*
		let firstName = prompt("What is your first name?");
		let lastName = prompt("What is your last Name?");
		let age = prompt("What is your age?");
		let address = {
			city: prompt("Which city do you live in?"),
			country: prompt("Which country do you belong to?")
		};


		let otherData = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			age: age,
			address: address
		});

		console.log(otherData);
	*/

// [SECTION] Converting stringified JSON into Javascript objects

	// This happens both for sending information to a backend application and sending information back to a frontend application.

		let batchesJSON = `[{"batchName" : "Batch X"}, {"batchName" : "Batch Y"} ]`;
		console.log(batchesJSON);
		console.log("Result from Parse method:");
		console.log(JSON.parse(batchesJSON));

	// Mini-Activity

	// Parse the following into a Javascript object:

	let stringifiedObject = `{
		"name": "Lucifer",
		"age": 19,
		"address": {
			"city": "Mumbai",
			"country": "India"
		
	}}`;

	console.log(JSON.parse(stringifiedObject));























