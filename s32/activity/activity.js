let http = require('http');

const port = 4002;

let server = http.createServer(function(request, response){

	if(request.url == '/' && request.method == 'GET'){

		response.writeHead(200, {'content-Type' : 'text/plain'});
		response.write('Welcome to Booking System');
		response.end();
	}

	if(request.url == '/profile' && request.method == 'GET'){

		response.writeHead(200, {'content-Type' : 'text/plain'});
		response.write('Welcome to your profile!');
		response.end();
	}

	if(request.url == '/courses' && request.method == 'GET'){

		response.writeHead(200, {'content-Type' : 'text/plain'});
		response.write("Here's our courses available");
		response.end();
	}

	if(request.url == '/addCourse' && request.method == 'POST'){

		response.writeHead(200, {'content-Type' : 'text/plain'});
		response.write("Add course to resources");
		response.end();
	}

	if(request.url == '/updateCourse' && request.method == 'PUT'){

		response.writeHead(200, {'content-Type' : 'text/plain'});
		response.write("Update a course to our resources");
		response.end();
	}

	if(request.url == '/archiveCourse' && request.method == 'DELETE'){

		response.writeHead(200, {'content-Type' : 'text/plain'});
		response.write("Archive courses to our resources");
		response.end();
	}

})

server.listen(port);

console.log('Server is now running successfully on port 4002');














































