let http =  require('http');

// Mock Database

let directory = [
	{
		'name': 'Brandon',
		'email': 'brandon@mail.com'
	},
	{
		'name': 'Robert',
		'email': 'robert@mail.com'
	}
]

http.createServer(function(request,response) {
	if (request.url == '/users' && request.method == 'GET'){

		response.writeHead(200, {'content-Type': 'application/json'});
		console.log(directory);

		// Sers the response output to JSON data type
		// We will convert the users array into JSON since the server return response in JSON format as well.
		response.write(JSON.stringify(directory));
		response.end();
	}

	if (request.url == '/users' && request.method == 'POST'){

		// 1. Initiate requestBody variable which will later contain the data/body from teh Postman
		let requestBody = '';

		// 2. Upon receiving the data, reassign the value of requestBody to teh contents of the body from postman
		// Data is received from the client and is processed in the "data" stream
		request.on('data', function(data){

			requestBody += data;
			console.log(requestBody);
		});

		// 3. Before the request ends, convert the requestbody variable from string into JS object in order to be able to access its properties and assign them into newUser variable
		request.on('end', function(){
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the mock database 
			let newUser = {
				'name' : requestBody.name,
				'email' : requestBody.email
			};

			// After setting the values from the requestBody to the newUser variable, push the newUser variable into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'content-Type': 'application/json' });
			response.write(JSON.stringify(newUser));
			response.end();
		})
	}

}).listen(4001);

console.log('Server is now successfully running at localhost:4001');








































