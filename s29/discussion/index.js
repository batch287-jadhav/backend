// [SECTION] Comparison Query Operators

// $gt/$gte
	// Allows us to finde documents that have field number values greater than or equal to a specified value.

	db.users.find({age: {$gt : 50}}).pretty();
	// here preety() makes the results more readable

	db.users.find({age: {$gte : 50}}).pretty();

// $lt/$lte
	// Allows us to finde documents that have field number values less than or equal to a specified value.

	db.users.find({age: {$lt : 50}}).pretty();
	// here preety() makes the results more readable

	db.users.find({age: {$lte : 50}}).pretty();

// $ne(Not Equal)
	// Allows us to find documents that have feild values that are not equal to a specified value
	db.users.find({age: {$ne : 82}}).pretty();

// $in
	// Allows us to find documents with specific match criteria using different values
	db.users.find({lastName: {$in : ['Hawking','Doe']}}).pretty();
	db.users.find({courses: {$in : ['HTML','React']}}).pretty();

// [SECTION] Logical Query Operators
	
	// $or operator
		// Allows us to find documents that match a single criteria from multiple provided search criteria

		db.users.find({ $or: [{firstName: 'Neil'},{age: '25'}]}).pretty();

		db.users.find({ $or: [ { firstName: 'Neil'}, {age : {$gt : 30}}]}).pretty();

	// $and operator
		// Allows us to find document matching multiple criteria in a single feild

		db.users.find({ $and: [{age: { $ne:82}}, {age: {$ne:76}} ]).pretty();

// [SECTION] Feild Projection
	/* 	
		- Retriving documents are common operations that we do and by default MongoDB queries return the whole document as a response
		- When dealing with complex data structures, there might be instances when feilds are not useful for the query we are trying to achive
		- To help with readability of the values returned, we can include/exclude feilds from the response
	*/

	// Inclusion:
		/*
			- Allows us to indclude/add specific fields only when retrieving documents
			- The value provided is 1 to denote that the field is being included
		*/

	db.users.find(
		{
			firstName: 'Jane'
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	).pretty();

	// Exlusion:
		/*
			- Allows us to exclude/remove specific fields only when retrieving documents
			- The value provided is 0 to denote that the field is being excluded
		*/
	db.users.find(
		{
			firstName: 'Jane'
		},
		{
			lastName: 0
		}
	).pretty();

	// Suppressing the ID feild
		/*
			- Allows us to exclude the "_id" feild when retrieving documents
			- When using feild projection, fiedl inclusion and exclusion may not be used at the same time
			- Excluding the "_id" feild is the only exception for this
		*/

	db.users.find(
		{
			firstName: 'Jane'
		},
		{
			firstName: 1,
			lastName: 1,
			_id : 0
		}
	).pretty();


	// Returning Specific feilds in embeded documents

	db.users.find(
		{
			firstName: 'Jane'
		},
		{
			firstName:1,
			lastName:1,
			"contact.phone":1
		}
	).pretty();

	// Supressing specific feilds in embedded documents

	db.users.find(
		{
			firstName: 'Jane'
		},
		{
			"contact.phone": 0
		}
	).pretty();

	// Project Specific Array Elements in the Returned Array
		// The $slice operator allows us to retrieve only 1 element that matches the search criteria

	db.users.find(
		{ "namearr":
			{
				nameA : 'Juan'
			}
		},
		{ namearr:
			{
				$slice: 1
			}
		}
	).pretty();

// [SECTION] Evaluation Query Operators

	// $regex operator
		/*
			- Allows us to find documents that match a specific string pattern using regular expressions
		*/

	// Case Sensitive query
	db.users.find({firstName: {$regex: 'N'}}).pretty();

	// Case Insensitive query
	db.users.find({firstName: {$regex: 'j', $options: 'i'} }).pretty();

