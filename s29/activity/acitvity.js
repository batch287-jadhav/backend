// Find users with letter s in their firstName and 'd' in their lastName

	db.users.find({
		$or: [{firstName: {$regex: 's', $options: 'i'}}, {lastName: {$regex: 'd', $options: 'i'}}]
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	});


// Find users who are from the 'HR' Department and age greater than equal to 70.

	db.users.find(
		{
			$and : [{department: 'HR'},{ age: { $gte: 70}}]	

	});

// Find users with 'e' in their first name and age less than equal to 30.

	db.users.find(
		{
			$and: [{firstName: {$regex: 'e', $options: 'i'}}, {age: { $lte: 30}}]
		});
	






























